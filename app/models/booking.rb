class Booking < ActiveRecord::Base
  belongs_to :band
  belongs_to :club

  attr_accessible :band, :club, :band_id, :club_id, :fee, :show_time
end
