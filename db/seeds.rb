# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Band.delete_all
Booking.delete_all
Club.delete_all

Band.create([{name: "Primus", num_members: "3"},{name: "311", num_members: "5"}, {name: "Hank 3", num_members: "5" }, {name: "Pink Floyd", num_members: "4" }])

Club.create([{name: "House of Blues", street_address: "1204 Caroline St, Houston, TX 77002" }, {name: "Bayou Music Center", street_address: "520 Texas St, Houston, TX 77002"}, {name: "Toyota Center", street_address: "1510 Polk St, Houston, TX 77002"}])

Booking.create(band: Band.first, club: Club.first, fee:80, show_time:'28/09/2013')
Booking.create(band: Band.first, club: Club.find_by_name("Bayou Music Center"), fee:80, show_time:'27/09/2013')
Booking.create(band: Band.find_by_name("Hank 3"), club: Club.find_by_name("Bayou Music Center"), fee:25, show_time: '30/9/2013')
Booking.create(band: Band.find_by_name("Hank 3"), club: Club.find_by_name("Toyota Center"), fee:75, show_time: '04/10/2013')
Booking.create(band: Band.find_by_name("311"), club: Club.first, fee:50, show_time: '11/10/2013')
Booking.create(band: Band.find_by_name("311"), club: Club.find_by_name("Bayou Music Center"), fee:50, show_time: '12/10/2013')
Booking.create(band: Band.find_by_name("311"), club: Club.last, fee:80, show_time: '12/10/2013')
Booking.create(band: Band.last, club: Club.last, fee:275, show_time:'10/05/2013')
Booking.create(band: Band.last, club: Club.find_by_name("Bayou Music Center"), fee:275, show_time:'25/10/2013')

