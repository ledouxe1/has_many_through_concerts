class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.references :band
      t.references :club
      t.float :fee
      t.datetime :show_time

      t.timestamps
    end
  end
end
